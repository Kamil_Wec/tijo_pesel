package pl.edu.pwsztar

import spock.lang.Specification

class CheckSexSpec extends Specification {
    def "should check person sex"(){
        given:
        UserId man = new UserId("74351202352")
        UserId woman = new UserId("74351202345")
        UserId empty = new UserId("74351202346")
        when:
        Optional<UserIdChecker.Sex> s1 = man.getSex();
        Optional<UserIdChecker.Sex> s2 = woman.getSex();
        Optional<UserIdChecker.Sex> s3 = empty.getSex();
        then:
        s1.get() == UserIdChecker.Sex.MAN;
        s2.get() == UserIdChecker.Sex.WOMAN;
        s3 == Optional.empty()
    }
}
