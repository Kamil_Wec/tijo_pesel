package pl.edu.pwsztar;

import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL


    public UserId(final String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean isCorrectSize() {
        if(getId() == null || getId().equals("")){
            return false;
        }
        return getId().length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if(!isCorrect()){
            return Optional.empty();
        }
        int i = Integer.parseInt(String.valueOf(getId().charAt(9)));
        if(i % 2 == 0) {
            return Optional.of(Sex.WOMAN);
        }else{
            return Optional.of(Sex.MAN);
        }
    }

    @Override
    public boolean isCorrect() {
        String pesel = getId();
        int a = Integer.parseInt(String.valueOf(pesel.charAt(0)));
        int b = Integer.parseInt(String.valueOf(pesel.charAt(1)));
        int c = Integer.parseInt(String.valueOf(pesel.charAt(2)));
        int d = Integer.parseInt(String.valueOf(pesel.charAt(3)));
        int e = Integer.parseInt(String.valueOf(pesel.charAt(4)));
        int f = Integer.parseInt(String.valueOf(pesel.charAt(5)));
        int g = Integer.parseInt(String.valueOf(pesel.charAt(6)));
        int h = Integer.parseInt(String.valueOf(pesel.charAt(7)));
        int i = Integer.parseInt(String.valueOf(pesel.charAt(8)));
        int j = Integer.parseInt(String.valueOf(pesel.charAt(9)));
        int k = Integer.parseInt(String.valueOf(pesel.charAt(10)));
        int sum = (9*a)+(7*b)+(3*c)+(1*d)+(9*e)+(7*f)+(3*g)+(1*h)+(9*i)+(7*j);
        System.out.println("Reszta z dzielenia " + sum%10);
        if((sum % 10) == k){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrect()){
            return Optional.empty();
        }

        String pesel = getId();

        int yearCheck = Integer.parseInt(String.valueOf(pesel.charAt(2)));
        String day = pesel.substring(4,6);
        String month = pesel.substring(3,4);
        int year = Integer.parseInt(pesel.substring(0,2));

        if(yearCheck == 0 || yearCheck == 1){
            month = yearCheck + month;
        }else{
            month = "0" + month;
        }
        if(yearCheck == 0 || yearCheck == 1){
            year += 1900;
        }else if(yearCheck == 2 || yearCheck == 3){
            year += 2000;
        }else if(yearCheck == 4 || yearCheck == 5){
            year += 2100;
        }else if(yearCheck == 6 || yearCheck == 7){
            year += 2200;
        }else if(yearCheck == 8 || yearCheck == 9){
            year += 1800;
        }

        return Optional.of(day + "-" + month + "-" + year);
    }
}
