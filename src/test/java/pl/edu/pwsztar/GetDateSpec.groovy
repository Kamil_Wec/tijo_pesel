package pl.edu.pwsztar

import spock.lang.Specification

class GetDateSpec extends Specification{
    def "should return date from id" (){
        given: "initial data"

        UserId userId = new UserId("74351202345");
        UserId userId2 = new UserId("97112156437");
        UserId userId3 = new UserId("65850165879");
        UserId userId4 = new UserId("74030412546");

        when: "call function"
        String date = userId.getDate().get()
        String date2 = userId2.getDate().get()
        String date3 = userId3.getDate().get()
        Optional<String> date4 = userId4.getDate()

        then: "check if correct"
        date.equals("12-05-2074")
        date2.equals("21-11-1997")
        date3.equals("01-05-1865")
        date4 == Optional.empty()

    }
}
