package pl.edu.pwsztar

import spock.lang.Specification

class CheckSizeSpec extends Specification {


    def "should check correct size"() {
        given:
        UserId userId = new UserId("87040323456")
        when:
        boolean correct = userId.isCorrectSize()
        then:
        correct
    }
    def "should check incorrect size"() {
        given:
        UserId userId = new UserId("870403234562")
        when:
        boolean correct = userId.isCorrectSize()
        then:
        !correct
    }
    def "should check null"() {
        given:
        UserId userId = new UserId(null)
        when:
        boolean correct = userId.isCorrectSize()
        then:
        !correct
    }
    def "should check empty string"() {
        given:
        UserId userId = new UserId("")
        when:
        boolean correct = userId.isCorrectSize()
        then:
        !correct
    }
}
